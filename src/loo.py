#!/usr/bin/env python3

import toml
import time
import asyncio
import simpleobsws
import sys
import logging
logging.basicConfig(level=logging.DEBUG)

parameters = simpleobsws.IdentificationParameters(ignoreNonFatalRequestChecks=False)
ws = simpleobsws.WebSocketClient(url='ws://localhost:4455',
                                 password='changeme',
                                 identification_parameters=parameters)

def loopy_loop():
    """
    The main loop for loopy.

    We purposely re-load the configuration at each cycle, so that changes
    made to loopy will reflect on its next cycle.
    """
    loopy = toml.load("loopy.toml")

    print("Welcome to %s" % loopy["loopy"]["description"])
    print("Running sequence: %s" % loopy["loopy"]["sequence"])

    for sequence_item in loopy["loopy"]["sequence"].split(" "):
        print("In sequence item %s, the following scenes are enabled: %s"
              % (sequence_item, loopy[sequence_item]["enabled"]))

        for scene in loopy[sequence_item]["enabled"].split(" "):
            print(" - Switching to scene %s for %s seconds..."
                  % (scene, loopy[sequence_item][scene]["duration"]))
            #change_scene(scene)
            loop = asyncio.get_event_loop()
            loop.run_until_complete(change_scene(scene))
            time.sleep(loopy[sequence_item][scene]["duration"])


async def change_scene(scene):
    """
    Change a scene in OBS.
    """
    await ws.connect()
    await ws.wait_until_identified()

    request = simpleobsws.Request('SetCurrentProgramScene',
                                  {'sceneName': scene})

    ret = await ws.call(request)
    if ret.ok():
        print("Request succeeded! Response data: {}".format(ret.responseData))

    await ws.disconnect()




while True:
    loopy_loop()
